import time, shutil, os

def bk_folder(period_time, source, target):
    aux = 0

    source_ = os.path.basename(os.path.normpath(source))
    
    while True:
        time.sleep(period_time)
        target_folder = target + "/" + source_ + "_" + time.strftime("%Y-%m-%d_%H-%M-%S")
        shutil.copytree(source, target_folder)

        aux += 1

        #msg = "Backup #{} created".format(aux)
