# coding: utf-8

import sys, multiprocessing, signal, qdarkstyle

from qtpy           import QtWidgets
from qtpy.QtWidgets import QApplication, QMainWindow

from qdarkstyle.dark.palette  import DarkPalette
from qdarkstyle.light.palette import LightPalette

from library.functions import *

from ui.mainwindow import Ui_MainWindow

appname = "Incremental Folder Backup"

about = appname + " Version 1.0\n\nThis program makes an incremental copy \
of the folder"
    
authors = ["Luis Acevedo", "<laar@pm.me>"]

license_ = "Copyright 2020. All code is copyrighted by the respective authors.\n" \
+ appname + " can be redistributed and/or modified under the terms of \
the GNU GPL versions 3 or by any future license endorsed by " + authors[0] + \
".\nThis program is distributed in the hope that it will be useful, but \
WITHOUT ANY WARRANTY; without even the implied warranty of \
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE."

folders = {
    "source": "",
    "target": ""
}

process_bk_folder = None

class MainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setupUi(self)
        self.connectSignalsSlots()

    def connectSignalsSlots(self):
        # About
        self.actionAbout.triggered.connect(self.about_)

        # About Qt
        self.actionAbout_Qt.triggered.connect(self.aboutQt)

        # Authors
        self.actionAuthors.triggered.connect(self.authors)

        # License
        self.actionLicense.triggered.connect(self.license_)

        # Change theme
        self.btn_theme.setStyleSheet(
            "QPushButton { background-color: purple; } \
                QPushButton::hover { \
                background-color: grey; \
            }"
        )
        self.btn_theme.setCheckable(True)
        #self.btn_theme.setChecked(True)
        self.btn_theme.clicked.connect(self.toggle_theme)

        # Exit
        self.btn_exit.clicked.connect(self.exit)

        # Clear list button
        self.btn_clear.clicked.connect(self.clear)

        # Select source
        self.btn_source.clicked.connect(self.source)

        # Select target
        self.btn_target.clicked.connect(self.target)

        # Backup
        self.btn_bk.clicked.connect(self.backup)
        self.btn_bk.setStyleSheet(
            "QPushButton { background-color: green; } \
                QPushButton::hover { \
                background-color: grey; \
            }"
        )

    def about_(self):
        QtWidgets.QMessageBox.about(self, "About", about)

    def aboutQt(self):
        QtWidgets.QMessageBox.aboutQt(self)
    
    def authors(self):
        text = authors[0] + " " + authors[1]
        QtWidgets.QMessageBox.about(self, "Authors", text)
        
    def license_(self):
        QtWidgets.QMessageBox.about(self, "License", license_)

    def toggle_theme(self):
        if not self.btn_theme.isChecked():
            app.setStyleSheet(qdarkstyle.load_stylesheet(qt_api="pyside2", palette=DarkPalette))
        else:
            app.setStyleSheet(qdarkstyle.load_stylesheet(qt_api="pyside2", palette=LightPalette))

    def exit(self):
        self.stop_bk()
        sys.exit()

    def clear(self):
        self.folder_info.setText("")
        globals()["folders"]["source"] = ""
        globals()["folders"]["target"] = ""
        #folders.clear() # For a list

    def source(self):
        directory = QtWidgets.QFileDialog.getExistingDirectory()
        globals()["folders"]["source"] = directory

        text = "Source: {}\n\nTarget: {}".format(
            globals()["folders"]["source"],
            globals()["folders"]["target"]
        )
        self.folder_info.setText(text)
        
    def target(self):
        directory = QtWidgets.QFileDialog.getExistingDirectory()
        globals()["folders"]["target"] = directory

        text = "Source: {}\n\nTarget: {}".format(
            globals()["folders"]["source"],
            globals()["folders"]["target"]
        )
        self.folder_info.setText(text)

    def backing_up_info(self):
        info = "Process info: {}\n".format(
            str(globals()["process_bk_folder"])
        )

        more_info = "Backing up:\n{}\nto\n{}\nEvery {} seconds".format(
            globals()["folders"]["source"],
            globals()["folders"]["target"],
            self.period_time.value()
        )
        
        msgBox = QtWidgets.QMessageBox()
        msgBox.setText(info)
        msgBox.setInformativeText(more_info)
        msgBox.setWindowTitle("BACKING UP")
        
        stop_btn = msgBox.addButton("Stop", QtWidgets.QMessageBox.YesRole)
        stop_btn.setStyleSheet(
            "QPushButton { background-color: green; } \
                QPushButton::hover { \
                background-color: grey; \
            }"
        )
        
        exit_btn = msgBox.addButton("Exit", QtWidgets.QMessageBox.NoRole)
        exit_btn.setStyleSheet(
            "QPushButton { background-color: red; } \
                QPushButton::hover { \
                background-color: grey; \
            }"
        )
        
        msgBox.exec_()

        if msgBox.clickedButton() == stop_btn:
            self.stop_bk()
        if msgBox.clickedButton() == exit_btn:
            self.exit()
            
    def no_folder_error(self):
        error = "Source folder and target folder must be specified"
        
        msgBox = QtWidgets.QMessageBox()
        msgBox.setIcon(QtWidgets.QMessageBox.Critical)
        msgBox.setText(error)
        #msgBox.setInformativeText("More information")
        msgBox.setWindowTitle("ERROR")
        msgBox.exec_()

    def period_time_error(self):
        error = "Period time must be at least 1 second"
        
        msgBox = QtWidgets.QMessageBox()
        msgBox.setIcon(QtWidgets.QMessageBox.Critical)
        msgBox.setText(error)
        msgBox.setWindowTitle("ERROR")
        msgBox.exec_()

    def backup(self):
        if len(globals()["folders"]["source"]) == 0 or len(globals()["folders"]["target"]) == 0:
            self.no_folder_error()
        elif self.period_time.value() == 0:
            self.period_time_error()
        else:
            globals()["process_bk_folder"] = multiprocessing.Process(
                target = bk_folder,
                args = (
                    self.period_time.value(),
                    globals()["folders"]["source"],
                    globals()["folders"]["target"],
                )
            )
            globals()["process_bk_folder"].start()
            self.backing_up_info()

    def stop_bk(self):
        if globals()["process_bk_folder"] is not None:
            if globals()["process_bk_folder"].is_alive():
                globals()["process_bk_folder"].terminate()
                globals()["process_bk_folder"].exitcode == -signal.SIGTERM
                self.clear()

if __name__ == "__main__":
    
    print("\n" + appname + " Copyright (C) 2020 " + authors[0] + ".\nThis program comes with ABSOLUTELY NO WARRANTY.\nThis is free software, and you are welcome to redistribute it\nunder certain conditions.\nplease, refer to README file.")

    app = QApplication(sys.argv)
    app.setStyleSheet(qdarkstyle.load_stylesheet(qt_api="pyside2", palette=DarkPalette))

    window = MainWindow()
    window.show()
    sys.exit(app.exec_())
